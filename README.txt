The ImageCache Use Original module solves the problem where if the image 
is already in the correct resolution and compression, imagecache processes
that image anyway.

In some cases we do need to change the image scales, which imagecache does well.
But in many cases the images are already in the correct resolution and the
problem is imagecache is still regenerating those images with the results being
worse quality and larger file sizes.
The solution: If the file already has the correct dimensions of the current 
preset, only copy the file into the directory of that imagecache preset instead 
of allowing imagecache to process the image.

Installing ImageCache Use Original

As of this version, the following patch needs to be applied to the ImageCache
module:
https://drupal.org/node/2114181#comment-7975369
